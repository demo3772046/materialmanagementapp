<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\InwardOutward;
use App\Models\Material;

class InwardOutwardController extends Controller
{

    public function index()
    {
        $inwardOutwards = InwardOutward::all();
        return view('inward_outwards.index', compact('inwardOutwards'));
    }

    public function create()
    {
        $materials = Material::all();
        return view('inward_outwards.create', compact('materials'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'material_id' => 'required|exists:materials,id',
            'date' => 'required|date',
            'quantity' => 'required|numeric',
        ]);

        InwardOutward::create($request->all());

        return redirect()->route('inwardoutwards.index')->with('success', 'Inward/Outward Quantity added successfully.');
    }

}
