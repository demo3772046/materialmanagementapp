<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Material;
use App\Models\Category;

class MaterialController extends Controller
{

    public function index()
    {
        $materials = Material::all();
        return view('materials.index', compact('materials'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('materials.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string',
            'opening_balance' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
        ]);

        Material::create($request->all());

        return redirect()->route('materials.index')->with('success', 'Material added successfully.');
    }

    public function edit($id)
    {
        $material = Material::findOrFail($id);
        $categories = Category::all();
        return view('materials.edit', compact('material', 'categories'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'opening_balance' => 'required|numeric',
            'category_id' => 'required|exists:categories,id',
        ]);

        $material = Material::findOrFail($id);
        $material->update($request->all());

        return redirect()->route('materials.index')->with('success', 'Material updated successfully.');
    }

   public function destroy($id)
    {
        $material = Material::findOrFail($id);
        $material->delete();
        return redirect()->route('materials.index')->with('success', 'Material soft deleted successfully.');
    }

}
