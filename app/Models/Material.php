<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
class Material extends Model
{
    use SoftDeletes;

    protected $dates = ['deleted_at'];

    protected $fillable = ['name', 'opening_balance', 'category_id'];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function inwardOutwards()
    {
        return $this->hasMany(InwardOutward::class);
    }

    public function calculateCurrentBalance()
    {
        $totalInwardQuantity = $this->inwardOutwards()->sum('quantity');
        return $this->opening_balance + $totalInwardQuantity;
    }
}
