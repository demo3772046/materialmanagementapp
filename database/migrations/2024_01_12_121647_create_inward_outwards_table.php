<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('inward_outwards', function (Blueprint $table) {
        $table->id();
        $table->unsignedBigInteger('material_id');
        $table->date('date');
        $table->float('quantity', 10, 2);
        $table->timestamps();

        $table->foreign('material_id')->references('id')->on('materials');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('inward_outwards');
    }
};
