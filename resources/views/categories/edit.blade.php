@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Category</div>
                    <div class="card-body">
                        <form action="{{ route('categories.update', $category->id) }}" method="post">
                            @csrf
                            @method('PATCH')

                            <div class="mb-3">
                                <label for="name" class="form-label">Category Name:</label>
                                <input type="text" name="name" class="form-control" value="{{ $category->name }}" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Update Category</button>
                             <a href="{{ route('categories.index') }}" class="btn btn-secondary">Back to Categories</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
   
@endsection
