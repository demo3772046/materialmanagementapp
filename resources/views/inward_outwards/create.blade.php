@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Inward Outward Quantity</div>
                    <div class="card-body">
                        <form action="{{ route('inwardoutwards.store') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="material_id" class="form-label">Material:</label>
                                <select name="material_id" class="form-select" required>
                                    @foreach($materials as $material)
                                        <option value="{{ $material->id }}">{{ $material->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="date" class="form-label">Date:</label>
                                <input type="date" name="date" class="form-control" required>
                            </div>

                            <div class="mb-3">
                                <label for="quantity" class="form-label">Quantity:</label>
                                <input type="number" name="quantity" class="form-control" step="0.01" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Save</button>
                             <a href="{{ route('inwardoutwards.index') }}" class="btn btn-secondary">Back to Inward/Outward Quantities</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

   
@endsection
