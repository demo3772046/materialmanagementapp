<!-- resources/views/inwardoutwards/index.blade.php -->

@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h1>Add Inward/Outward Quantity</h1>
    <div>
        <a href="{{ route('inwardoutwards.create') }}" class="btn btn-success">Add Inward Outward</a>
    </div>
</div>
    <table class="table table-striped table-bordered zero-configuration">
        <thead>
            <tr>
                <th>Material category</th>
                <th>Material name</th>
                <th>Date</th>
                <th>Quantity</th>
               
            </tr>
        </thead>
        <tbody>
            @foreach($inwardOutwards as $inwardOutward)
                <tr>
                    <td>{{ $inwardOutward->material->category->name }}</td>
                    <td>{{ $inwardOutward->material->name }}</td>
                    <td>{{ $inwardOutward->date }}</td>
                    <td>{{ $inwardOutward->quantity }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
   
@endsection
