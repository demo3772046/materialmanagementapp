<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Material Management App</title>
    <!-- Include Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Include any additional CSS styles if needed -->

    <style>
        /* Custom style for the selected li with bold */
        .navbar-nav .nav-item.active a {
            font-weight: bold;
        }
    </style>
</head>
<body>
    <header>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ route('materials.index') }}">Material Management App</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav">
                        <li class="nav-item {{ request()->is('materials*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('materials.index') }}">Materials</a>
                        </li>
                        <li class="nav-item {{ request()->is('categories*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('categories.index') }}">Categories</a>
                        </li>
                        <li class="nav-item {{ request()->is('inwardoutwards*') ? 'active' : '' }}">
                            <a class="nav-link" href="{{ route('inwardoutwards.index') }}">Inward/Outward</a>
                        </li>
                        <!-- Add links for other sections as needed -->
                    </ul>
                </div>
            </div>
        </nav>
    </header>

    <main class="container mt-4">
        @yield('content')
    </main>

    <footer class="mt-4 text-center">
        <!-- Footer content goes here -->
    </footer>

    <!-- Include Bootstrap JS and Popper.js -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"></script>
    <!-- Include any additional JS scripts or external libraries -->
</body>
</html>
