@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Add Material</div>
                    <div class="card-body">
                        <form action="{{ route('materials.store') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="category_id" class="form-label">Material Category:</label>
                                <select name="category_id" class="form-select" required>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="name" class="form-label">Material Name:</label>
                                <input type="text" name="name" class="form-control" required>
                            </div>

                            <div class="mb-3">
                                <label for="opening_balance" class="form-label">Opening Balance:</label>
                                <input type="number" name="opening_balance" class="form-control" step="0.01" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Save Material</button>
                              <a href="{{ route('materials.index') }}" class="btn btn-secondary">Back to Materials</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
@endsection
