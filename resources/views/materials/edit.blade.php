@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Edit Material</div>
                    <div class="card-body">
                        <form action="{{ route('materials.update', $material->id) }}" method="post">
                            @csrf
                            @method('PATCH')

                            <div class="mb-3">
                                <label for="category_id" class="form-label">Material Category:</label>
                                <select name="category_id" class="form-select" required>
                                    @foreach($categories as $category)
                                        <option value="{{ $category->id }}" {{ $material->category_id == $category->id ? 'selected' : '' }}>
                                            {{ $category->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="mb-3">
                                <label for="name" class="form-label">Material Name:</label>
                                <input type="text" name="name" class="form-control" value="{{ $material->name }}" required>
                            </div>

                            <div class="mb-3">
                                <label for="opening_balance" class="form-label">Opening Balance:</label>
                                <input type="number" name="opening_balance" class="form-control" step="0.01" value="{{ $material->opening_balance }}" required>
                            </div>

                            <button type="submit" class="btn btn-primary">Update Material</button>
                               <a href="{{ route('materials.index') }}" class="btn btn-secondary">Back to Materials</a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
@endsection
