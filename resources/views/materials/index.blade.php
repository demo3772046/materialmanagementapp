@extends('layouts.app')

@section('content')
<div class="d-flex justify-content-between align-items-center mb-3">
    <h1>All Materials</h1>
    <div>
        <a href="{{ route('materials.create') }}" class="btn btn-success">Add Material</a>
    </div>
</div>
    <table class="table table-striped table-bordered zero-configuration">
        <thead>
            <tr>
                <th>Material category</th>
                <th>Material name</th>
                <th>Opening balance</th>
                <th>Current balance</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($materials as $material)
                <tr>
                    <td>{{ $material->category->name }}</td>
                    <td>{{ $material->name }}</td>
                    <td>{{ $material->opening_balance }}</td>
                    <td>{{ $material->calculateCurrentBalance() }}</td>
                    <td><a href="{{ route('materials.edit', $material->id) }}" class="btn btn-primary">Edit</a>
                        <form action="{{ route('materials.destroy', $material->id) }}" class="d-inline" method="post">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger ml-2">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    
@endsection
